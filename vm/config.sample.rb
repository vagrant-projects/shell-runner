VM_NAME = "shell-runner"
VM_DOMAIN = "shell-runner.test"
VM_IP = "172.16.245.100"
#VM_PUB_IP = "192.168.3.122"
VM_MEMORY = 8192
VM_CPUS = 3
#VM_MEMORY = 24576
#VM_CPUS = 8
# ----- GitLab ---
GITLAB_URL = "https://gitlab.com/"
GITLAB_IP = "0.0.0.0"
#GITLAB_TOKEN = "token"
RUNNER_TAG = "vagrant"
RUNNER_NAME = "shell-runner"